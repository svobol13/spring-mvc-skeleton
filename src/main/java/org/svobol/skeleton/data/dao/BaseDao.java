package org.svobol.skeleton.data.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;


/**
 * Convenient Dao super class
 *
 * @author Lukas Svoboda
 * @param <T>
 */
public abstract class BaseDao<T> {

    /**
     * Get list of entities matching given example.
     * @param example
     * @return list
     */
    @Transactional(readOnly=true)
    List<T> findByExample(T example) {
        return null;
    }

    /**
     * Attempts to merge given entity with database.
     * @param entity
     * @return merged
     * @see Session#merge(Object)
     */
    @Transactional
    T merge(T entity) {
        return null;
    }

    /**
     * Attempts to find entity by the given id.
     * @param id
     * @return
     */
    @Transactional(readOnly=true)
    T findById(Serializable id) {
        return null;
    }

    /**
     * Attempts to delete entity with the given id
     * @param id
     * @return entity Deleted entity.
     */
    @Transactional
    T delete(Serializable id) {
        return null;
    }

}
