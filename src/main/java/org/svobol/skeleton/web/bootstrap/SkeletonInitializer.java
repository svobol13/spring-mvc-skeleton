package org.svobol.skeleton.web.bootstrap;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.svobol.skeleton.web.bootstrap.config.MvcConfig;

/**
 * Main web application initializer. This is replacement for
 * web.xml configuration file in Servlet 3.0 environment.
 *
 * @author Lukas Svoboda
 */
public class SkeletonInitializer implements WebApplicationInitializer {

    private static final Class<?>[] annotatedClasses = new Class<?>[] { MvcConfig.class };

    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {
        // Initialize Spring annotation driven context
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(annotatedClasses);
        // Register DispatcherServlet
        DispatcherServlet dispatcherServlet = new DispatcherServlet(context);
        Dynamic dynamic = servletContext.addServlet("dispatcher", dispatcherServlet);
        dynamic.addMapping("/");
        dynamic.setLoadOnStartup(1);
    }

}
