package org.svobol.skeleton.web.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {

    private static final Logger log = LogManager.getLogger(IndexController.class);


    @RequestMapping({"index", ""})
    public String index() {
        log.debug("index page returned.");
        return "index";
    }

}
